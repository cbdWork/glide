## Glide

## Introduction:
Glide is a fast and efficient image loading library for openharmony focused on smooth scrolling. Glide offers an easy to use API, a performant and extensible resource decoding pipeline and automatic resource pooling.# Glide

Glide hmos testapplication + ported code project
Glide supports fetching, decoding, and displaying video stills, images, and animated GIFs. Glide includes a flexible api that allows developers to plug in to almost any network stack. By default Glide uses a custom HttpUrlConnection based stack, but also includes utility libraries plug in to Google's Volley project or Square's OkHttp library instead.

Glide primary focus is on making scrolling any kind of a list of images as smooth and fast as possible, but Glide is also effective for almost any case where you need to fetch, resize, and display a remote image.

## Usage Instructions:
API
Glide uses a simple fluent API that allows users to make most requests in a single line:


```
1)Load Image from Internet
 //String can be like "https://picsum.photos/600"
 Glide.with(classcontext)
                    .load(String)  
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(imageView);
                    
2)Load Image from Resource Folder
    //ResourceId can be esourceTable.Media_penguins
    Glide.with(classcontext)
            .load(ResourceId) 
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .into(imageView);
            
3) Uri uri =Uri.parse("https://picsum.photos/600");
   Glide.with(classcontext)
        .load(uri)
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true)
        .into(imageView);


4)Uri uri =Uri.parse("dataability:///" + "com.bumptech.glide"+ "/" + ResourceTable.Media_jellyfish);
  Glide.with(classcontext)
        .load(uri)
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true)
        .into(imageView);
```

## Installation instructions:

```
Method 1:
    Generate the .har package through the library and add the .har package to the libs folder.
    Add the following code to the entry gradle:
        implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])

Method 2:
    allprojects{
        repositories{
            mavenCentral()
        }
    }
implementation 'io.openharmony.tpc.thirdlib:glide:1.0.6'

```
