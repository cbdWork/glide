package com.bumptech.glide.manager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import ohos.app.Context;
import org.jetbrains.annotations.NotNull;

/**
 * A collection of static methods for creating new {@link com.bumptech.glide.RequestManager}s or
 * retrieving existing ones from activities and fragment.
 */
public class RequestManagerRetriever {
  static final String FRAGMENT_TAG = "com.bumptech.glide.manager";
  private static final String TAG = "RMRetriever";

  private static final int ID_REMOVE_FRAGMENT_MANAGER = 1;
  private static final int ID_REMOVE_SUPPORT_FRAGMENT_MANAGER = 2;

  // Hacks based on the implementation of FragmentManagerImpl in the non-support libraries that
  // allow us to iterate over and retrieve all active Fragments in a FragmentManager.
  private static final String FRAGMENT_INDEX_KEY = "key";

  /** The top application level RequestManager. */
  private volatile RequestManager applicationManager;
  private final RequestManagerFactory factory;

  public RequestManagerRetriever( RequestManagerFactory factory) {
    this.factory = factory != null ? factory : DEFAULT_FACTORY;

  }

  @NotNull
  
  private RequestManager getApplicationManager(
      @NotNull Context context) {
    // Either an application context or we're on a background thread.
    if (applicationManager == null) {
      synchronized (this) {
        if (applicationManager == null) {
          // Normally pause/resume is taken care of by the fragment we add to the fragment or
          // activity. However, in this case since the manager attached to the application will not
          // receive lifecycle events, we must force the manager to start resumed using
          // ApplicationLifecycle.

          // TODO(b/27524013): Factor out this Glide.get() call.
          Glide glide = Glide.get(context);
          applicationManager =
              factory.build(
                  glide,
                  new ApplicationLifecycle(),
                  new EmptyRequestManagerTreeNode(),
                  context);
        }
      }
    }

    return applicationManager;
  }

  
  @NotNull
  public RequestManager get(@NotNull Context context) {
    if (context == null) { // TODO Modified this API
      throw new IllegalArgumentException("You cannot start a load on a null Context");
    }

    return getApplicationManager(context); // TODO, rightnow passing null
  }


  /** Used internally to create {@link RequestManager}s. */
  public interface RequestManagerFactory {
	@NotNull
    RequestManager build(
           @NotNull Glide glide,
           @NotNull Lifecycle lifecycle,
           @NotNull RequestManagerTreeNode requestManagerTreeNode,
           @NotNull Context context);
  }

  private static final RequestManagerFactory DEFAULT_FACTORY =
      new RequestManagerFactory() {
        @NotNull
        @Override
        public RequestManager build(
             @NotNull Glide glide,
             @NotNull Lifecycle lifecycle,
             @NotNull RequestManagerTreeNode requestManagerTreeNode,
             @NotNull Context context) {
          return new RequestManager(glide, lifecycle, requestManagerTreeNode, context);
        }
      };
}
