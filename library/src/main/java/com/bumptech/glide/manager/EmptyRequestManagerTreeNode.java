package com.bumptech.glide.manager;


import com.bumptech.glide.RequestManager;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Set;

/** A {@link RequestManagerTreeNode} that returns no relatives. */
final class EmptyRequestManagerTreeNode implements RequestManagerTreeNode {
  @NotNull
  @Override
  public Set<RequestManager> getDescendants() {
    return Collections.emptySet();
  }
}
