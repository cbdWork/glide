package com.bumptech.glide.manager;

import org.jetbrains.annotations.NotNull;

/**
 * A {@link com.bumptech.glide.manager.Lifecycle} implementation for tracking and notifying
 * listeners of {@link ohos.aafwk.ability.AbilityPackage} lifecycle events.
 *
 * <p>Since there are essentially no {@link ohos.aafwk.ability.AbilityPackage} lifecycle events, this class
 * simply defaults to notifying new listeners that they are started.
 */
class ApplicationLifecycle implements Lifecycle {
  @Override
  public void addListener(@NotNull LifecycleListener listener) {
    listener.onStart();
  }

  @Override
  public void removeListener(@NotNull LifecycleListener listener) {
    // Do nothing.
  }
}
