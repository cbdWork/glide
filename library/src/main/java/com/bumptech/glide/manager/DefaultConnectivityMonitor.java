package com.bumptech.glide.manager;

import com.bumptech.glide.util.Synthetic;
import org.jetbrains.annotations.NotNull;
import ohos.app.Context;

/** Uses ConnectivityManager to identify connectivity changes. */
final class DefaultConnectivityMonitor implements ConnectivityMonitor {
  private static final String TAG = "ConnectivityMonitor";
  private final Context context;

  @SuppressWarnings("WeakerAccess")
  @Synthetic
  final ConnectivityListener listener;

  @SuppressWarnings("WeakerAccess")
  @Synthetic
  boolean isConnected;

  private boolean isRegistered;

  DefaultConnectivityMonitor(
          @NotNull Context context, @NotNull ConnectivityListener listener) {
    this.context = context;
    this.listener = listener;
  }

  private void register() {
    // Do nothing.
  }

  private void unregister() {
    // Do nothing.
  }

  @SuppressWarnings("WeakerAccess")
  @Synthetic
  // Permissions are checked in the factory instead.
  boolean isConnected(@NotNull Context context) {

    return false;
  }

  @Override
  public void onStart() {
    // Do nothing.
  }

  @Override
  public void onStop() {
    // Do nothing.
  }

  @Override
  public void onDestroy() {
    // Do nothing.
  }
}
