package com.bumptech.glide.manager;

/**
 * An interface for listener to {@link ohos.aafwk.ability.fraction.Fraction} and {@link ohos.aafwk.ability.Ability}
 * lifecycle events.
 */
public interface LifecycleListener {

  /**
   * Callback for when {@link ohos.aafwk.ability.fraction.Fraction#onStart()}} or {@link
   * ohos.aafwk.ability.Ability#onStart()} is called.
   */
  void onStart();

  /**
   * Callback for when {@link ohos.aafwk.ability.fraction.Fraction#onStop()}} or {@link
   * ohos.aafwk.ability.Ability#onStop()}} is called.
   */
  void onStop();

  /**
   * Callback for when {@link ohos.aafwk.ability.fraction.Fraction#onDestroy()}} or {@link
   * ohos.aafwk.ability.Ability#onDestroy()} is called.
   */
  void onDestroy();
}
