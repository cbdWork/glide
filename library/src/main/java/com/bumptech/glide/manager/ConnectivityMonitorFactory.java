package com.bumptech.glide.manager;

import ohos.app.Context;
import org.jetbrains.annotations.NotNull;

/**
 * A factory class that produces a functional {@link
 * com.bumptech.glide.manager.ConnectivityMonitor}.
 */
public interface ConnectivityMonitorFactory {

  @NotNull
  ConnectivityMonitor build(
          @NotNull Context context,
          @NotNull ConnectivityMonitor.ConnectivityListener listener);
}
