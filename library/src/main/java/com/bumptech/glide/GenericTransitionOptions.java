package com.bumptech.glide;

import org.jetbrains.annotations.NotNull;

/**
 * Implementation of {@link TransitionOptions} that exposes only generic methods that can be applied
 * to any resource type.
 *
 * @param <TranscodeType> The type of the resource that will be displayed.
 */
// Public API.
@SuppressWarnings({"PMD.UseUtilityClass", "unused"})
public final class GenericTransitionOptions<TranscodeType>
    extends TransitionOptions<GenericTransitionOptions<TranscodeType>, TranscodeType> {
  /**
   * Removes any existing animation put on the builder.
   *
   * @see GenericTransitionOptions#dontTransition()
   * @return generic transition options
   */
  @NotNull
  public static <TranscodeType> GenericTransitionOptions<TranscodeType> withNoTransition() {
    return new GenericTransitionOptions<TranscodeType>().dontTransition();
  }

  /**
   * Returns a typed {@link GenericTransitionOptions} object that uses the given view animation.
   *
   * @see GenericTransitionOptions#transition(int)
   * @param viewAnimationId
   * @return generic transition options
   */
  @NotNull
  public static <TranscodeType> GenericTransitionOptions<TranscodeType> with(int viewAnimationId) {
    return new GenericTransitionOptions<TranscodeType>().transition(viewAnimationId);
  }

}
