package com.bumptech.glide.signature;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.Util;
import org.jetbrains.annotations.NotNull;
import ohos.app.Context;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

/** Includes information about the package as well as whether or not the device is in night mode. */
public final class HarmonyResourceSignature implements Key {

  private final int nightMode;
  private final Key applicationVersion;

  @NotNull
  public static Key obtain( @NotNull Context context) {
    Key signature = ApplicationVersionSignature.obtain(context);
    int nightMode = 0; // TODO:
    return new HarmonyResourceSignature(nightMode, signature);
  }

  private HarmonyResourceSignature(int nightMode, Key applicationVersion) {
    this.nightMode = nightMode;
    this.applicationVersion = applicationVersion;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof HarmonyResourceSignature) {
      HarmonyResourceSignature that = (HarmonyResourceSignature) o;
      return nightMode == that.nightMode && applicationVersion.equals(that.applicationVersion);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return Util.hashCode(applicationVersion, nightMode);
  }

  @Override
  public void updateDiskCacheKey(@NotNull MessageDigest messageDigest) {
    applicationVersion.updateDiskCacheKey(messageDigest);
    byte[] nightModeData = ByteBuffer.allocate(4).putInt(nightMode).array();
    messageDigest.update(nightModeData);
  }
}
