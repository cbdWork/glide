package com.bumptech.glide.util;

import ohos.utils.LightweightMap;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("serial")
public final class CachedHashCodeArrayMap<K, V> implements Map<K, V> {

  private LightweightMap<K,V> lightmap;
  private int hashCode;

  public CachedHashCodeArrayMap(){
    lightmap = new LightweightMap<>();
  }

  @Override
  public int size(){ return lightmap.size();}

  @Override
  public boolean isEmpty(){ return lightmap.isEmpty();}

  @Override
  public boolean containsKey( Object var1){ return lightmap.containsKey(var1);}

  @Override
  public boolean containsValue( Object var1){ return lightmap.containsValue(var1);}

  @Override
  public V get(Object var1){ return lightmap.get(var1);}


  @Override
  public V remove( Object var1){ return lightmap.remove(var1);}


  @Override
  public Set<K> keySet(){ return lightmap.keySet();}

  @Override
  public Collection<V> values(){ return lightmap.values();}

  @Override
  public Set<Entry<K, V>> entrySet(){ return lightmap.entrySet();}

  @Override
  public boolean equals( Object var1){ return lightmap.equals(var1);}

  @Override
  public void clear() {
    hashCode = 0;
    lightmap.clear();
  }


  public V setValueAt(int index, V value) {
    hashCode = 0;
    return lightmap.setValueAt(index, value);
  }

  @Override
  public V put(K key, V value) {
    hashCode = 0;
    return lightmap.put(key, value);
  }

  @Override
  public void putAll(Map<? extends K, ? extends V> map) {
    hashCode = 0;
    lightmap.putAll(map);
  }


  public V removeAt(int index) {
    hashCode = 0;
    return lightmap.removeAt(index);
  }

  @Override
  public int hashCode() {
    if (hashCode == 0) {
      hashCode = lightmap.hashCode();
    }
    return hashCode;
  }

  public K keyAt(int index){
    return lightmap.keyAt(index);
  }

  public V valueAt(int index) {
    return lightmap.valueAt(index);
  }

}
