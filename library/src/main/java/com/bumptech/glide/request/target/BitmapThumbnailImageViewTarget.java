package com.bumptech.glide.request.target;

import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.media.image.PixelMap;

// Public API.
@SuppressWarnings("unused")
public class BitmapThumbnailImageViewTarget extends ThumbnailImageViewTarget<PixelMap> {
  public BitmapThumbnailImageViewTarget(Image view) {
    super(view);
  }

  /** @deprecated Use {@link #waitForLayout()} instead. */
  @SuppressWarnings("deprecation")
  @Deprecated
  public BitmapThumbnailImageViewTarget(Image view, boolean waitForLayout) {
    super(view, waitForLayout);
  }

  @Override
  protected Element getDrawable(PixelMap resource) {
    return null;
  }

}
