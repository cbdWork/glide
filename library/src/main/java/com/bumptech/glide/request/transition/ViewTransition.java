package com.bumptech.glide.request.transition;

import com.bumptech.glide.util.LogUtil;
import ohos.agp.animation.Animator;
import ohos.app.Context;

/**
 * A {@link Transition} that can apply a {@link ohos.agp.animation.Animator Animation} to a
 * {@link ohos.agp.components.Component View} using {@link
 * ohos.agp.components.Component#startAnimation(ohos.agp.animation.Animator)}.
 *
 * @param <R> The type of the resource that will be transitioned into a view.
 */
public class ViewTransition<R> implements Transition<R> {

  private final ViewTransitionAnimationFactory viewTransitionAnimationFactory;

  /**
   * Constructs a new ViewAnimation that will start the given {@link ohos.agp.animation
   * .Animation}.
   */
  ViewTransition(ViewTransitionAnimationFactory viewTransitionAnimationFactory) {
    this.viewTransitionAnimationFactory = viewTransitionAnimationFactory;
  }

  /**
   * Always clears the current animation on the view using {@link
   * ohos.agp.components.Component#clearAnimation()}, then starts the {@link ohos.agp.animation.Animator}
   * given in the constructor using {@link
   * ohos.agp.components.Component#startAnimation(ohos.agp.animation.Animator)} and then returns {@code
   * false} because the animation does not actually put the current resource on the view.
   *
   * @param current {@inheritDoc}
   * @param adapter {@inheritDoc}
   * @return {@inheritDoc}
   */
  @Override
  public boolean transition(R current, ViewAdapter adapter) {
   //TODO:openharmony
    return false;
  }

  interface ViewTransitionAnimationFactory {
    Animator build(Context context);
  }
}
