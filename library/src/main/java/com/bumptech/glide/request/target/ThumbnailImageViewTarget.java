package com.bumptech.glide.request.target;

import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import org.jetbrains.annotations.Nullable;

/**
 * Avoids extra calls to {@link Component#postLayout()} when loading more than once image
 * into an {@link Image} with fixed dimensions.
 *
 * <p>Typically it makes sense to use this class when loading multiple images with the {@link
 * com.bumptech.glide.RequestBuilder#thumbnail(com.bumptech.glide.RequestBuilder)} API into views in
 * a scrolling list like ListView, GridView, or RecyclerView.
 *
 * <p>{@link FixedSizeDrawable} may cause skewing or other undesirable behavior depending on your
 * images, views, and scaling. If this occurs, consider {@link DrawableImageViewTarget} or {@link
 * BitmapImageViewTarget} as alternatives.
 *
 * @param <T> The type of resource that will be displayed in the ImageView.
 */
// Public API.
@SuppressWarnings("WeakerAccess")
public abstract class ThumbnailImageViewTarget<T> extends ImageViewTarget<T> {

  public ThumbnailImageViewTarget(Image view) {
    super(view);
  }

  /** @deprecated Use {@link #waitForLayout()} insetad. */
  @Deprecated
  @SuppressWarnings({"deprecation"})
  public ThumbnailImageViewTarget(Image view, boolean waitForLayout) {
    super(view, waitForLayout);
  }

  @Override
  protected void setResource(@Nullable T resource) {
    //TODO:openharmony
  }

  protected abstract Element getDrawable(T resource);

}
