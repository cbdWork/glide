package com.bumptech.glide.request.target;

import ohos.agp.components.Image;
import ohos.media.image.PixelMap;

/** A {@link Target} that can display an {@link PixelMap} in an {@link Image}. */
public class BitmapImageViewTarget extends ImageViewTarget<PixelMap> {
  // Public API.
  @SuppressWarnings("WeakerAccess")
  public BitmapImageViewTarget(Image view) {
    super(view);
  }

  /** @deprecated Use {@link #waitForLayout()} instead. */
  // Public API.
  @SuppressWarnings({"unused", "deprecation"})
  @Deprecated
  public BitmapImageViewTarget(Image view, boolean waitForLayout) {
    super(view, waitForLayout);
  }

  @Override
  protected void setResource(PixelMap resource) {
    view.setPixelMap(resource);
  }
}
