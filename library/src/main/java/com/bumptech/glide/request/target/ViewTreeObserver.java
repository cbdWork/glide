package com.bumptech.glide.request.target;

/**
 * ViewTreeObserver
 */
public final class ViewTreeObserver {
  /** Interface definition for a callback to be invoked when the view tree is about to be drawn. */
  public interface OnPreDrawListener {
    /**
     * Callback method to be invoked when the view tree is about to be drawn. At this point, all
     * views in the tree have been measured and given a frame. Clients can use this to adjust their
     * scroll bounds or even to request a new layout before drawing occurs.
     *
     * @return Return true to proceed with the current drawing pass, or false to cancel.
     * @see ohos.agp.components.Component#onMeasure
     * @see ohos.agp.components.Component#onLayout
     * @see ohos.agp.components.Component#onDraw
     */
    public boolean onPreDraw();
  }
}
