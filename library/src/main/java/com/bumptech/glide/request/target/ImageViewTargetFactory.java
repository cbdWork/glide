package com.bumptech.glide.request.target;

import com.bumptech.glide.util.LogUtil;
import ohos.agp.components.Image;
import org.jetbrains.annotations.NotNull;

/**
 * A factory responsible for producing the correct type of {@link Target} for a given {@link
 * ohos.agp.components.Component} subclass.
 */
public class ImageViewTargetFactory {
	@NotNull
    @SuppressWarnings("unchecked")
    public <Z> ViewTarget<Image, Z> buildTarget(
            @NotNull Image view, @NotNull Class<Z> clazz) {

        // TODO
        return (ViewTarget<Image, Z>) new DrawableImageViewTarget(view);
    }
}
