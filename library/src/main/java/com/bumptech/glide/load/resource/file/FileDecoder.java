package com.bumptech.glide.load.resource.file;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.LogUtil;
import org.jetbrains.annotations.NotNull;

import java.io.File;

/**
 * A simple {@link com.bumptech.glide.load.ResourceDecoder} that creates resource for a given {@link
 * File}.
 */
public class FileDecoder implements ResourceDecoder<File, File> {

  @Override
  public boolean handles(@NotNull File source, @NotNull Options options) {
    return true;
  }

  @Override
  public Resource<File> decode(
      @NotNull File source, int width, int height, @NotNull Options options) {
    return new FileResource(source);
  }
}
