package com.bumptech.glide.load.resource;

import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.LogUtil;
import org.jetbrains.annotations.NotNull;
import ohos.app.Context;
import java.security.MessageDigest;

/**
 * A no-op Transformation that simply returns the given resource.
 *
 * @param <T> The type of the resource that will always be returned unmodified.
 */
public final class UnitTransformation<T> implements Transformation<T> {
  private static final Transformation<?> TRANSFORMATION = new UnitTransformation<>();

  /**
   * Returns a UnitTransformation for the given type.
   *
   * @param <T> The type of the resource to be transformed.
   * @return t
   */
  @SuppressWarnings("unchecked")
  @NotNull
  public static <T> UnitTransformation<T> get() {
    return (UnitTransformation<T>) TRANSFORMATION;
  }

  private UnitTransformation() {
    // Only accessible as a singleton.
  }

  @NotNull
  @Override
  public void updateDiskCacheKey(@NotNull MessageDigest messageDigest) {
    // Do nothing.
  }

  @Override
  public Resource<T> transform(@NotNull Context context, @NotNull Resource<T> resource, int outWidth, int outHeight) {
    return resource;
  }

  /* Use default implementations of equals and hashcode. */
}
