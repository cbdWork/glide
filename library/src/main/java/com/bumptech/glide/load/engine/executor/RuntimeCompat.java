package com.bumptech.glide.load.engine.executor;

import com.bumptech.glide.util.LogUtil;

/** Compatibility methods for {@link Runtime}. */
final class RuntimeCompat {
  private static final String TAG = "GlideRuntimeCompat";
  private static final String CPU_NAME_REGEX = "cpu[0-9]+";
  private static final String CPU_LOCATION = "/sys/devices/system/cpu/";

  private RuntimeCompat() {
    // Utility class.
  }

  /** Determines the number of cores available on the device.
   * @return int */
  static int availableProcessors() {
    int cpus = Runtime.getRuntime().availableProcessors();

    return cpus;
  }

  /**
   * Determines the number of cores available on the device (pre-v17).
   *
   * <p>Before Jellybean, {@link Runtime#availableProcessors()} returned the number of awake cores,
   * which may not be the number of available cores depending on the device's current state. See
   * https://stackoverflow.com/a/30150409.
   *
   * @return the maximum number of processors available to the VM; never smaller than one
   */
  @SuppressWarnings("PMD")
  private static int getCoreCountPre17() {
    return 0; //TODO:openharmony does not support Strictmode APIS
  }
}
