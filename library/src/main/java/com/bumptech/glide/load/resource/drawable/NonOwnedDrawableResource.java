package com.bumptech.glide.load.resource.drawable;

import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.LogUtil;
import ohos.agp.components.element.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Handles generic {@link Element} types where we may be uncertain of their size or type and where
 * we don't know that it's safe for us to recycle or re-use the Drawable.
 */
final class NonOwnedDrawableResource extends DrawableResource<Element> {

  @SuppressWarnings("unchecked")
  @Nullable
  static Resource<Element> newInstance( Element drawable) {
    return drawable != null ? new NonOwnedDrawableResource(drawable) : null;
  }

  private NonOwnedDrawableResource(Element drawable) {
    super(drawable);
  }

  
  @NotNull
  @SuppressWarnings("unchecked")
  @Override
  public Class<Element> getResourceClass() {
    return (Class<Element>) drawable.getClass();
  }

  @Override
  public int getSize() {
    // 4 bytes per pixel for ARGB_8888 Bitmaps is something of a reasonable approximation. If
    // there are no intrinsic bounds, we can fall back just to 1.
    //return Math.max(1, drawable.getIntrinsicWidth() * drawable.getIntrinsicHeight() * 4);
    return 0; //TODO:
  }

  @Override
  public void recycle() {
    // Do nothing.
  }
}
