package com.bumptech.glide.load.engine.bitmap_recycle;

import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import org.jetbrains.annotations.Nullable;


interface LruPoolStrategy {
  void put(PixelMap pixelmap);

  @Nullable
  PixelMap get(int width, int height, PixelFormat config);

  @Nullable
  PixelMap removeLast();

  String logBitmap(PixelMap pixelmap);

  String logBitmap(int width, int height, PixelFormat config);

  int getSize(PixelMap pixelmap);
}
