package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ImageHeaderParserUtils;
import com.bumptech.glide.load.data.DataRewinder;
import com.bumptech.glide.load.data.InputStreamRewinder;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.Util;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * This is a helper class for {@link Downsampler} that abstracts out image operations from the input
 * type wrapped into a {@link DataRewinder}.
 */
interface ImageReader {
  @Nullable
    PixelMap decodeBitmap(ImageSource.DecodingOptions options, String formatHint, boolean getimginfo) throws IOException;

    ImageHeaderParser.ImageType getImageType() throws IOException;

    int getImageOrientation() throws IOException;

    void stopGrowingBuffers();

    final class InputStreamImageReader implements ImageReader {
        private final InputStreamRewinder dataRewinder;
        private final ArrayPool byteArrayPool;
        private final List<ImageHeaderParser> parsers;

        InputStreamImageReader(
                InputStream is, List<ImageHeaderParser> parsers, ArrayPool byteArrayPool) {
            this.byteArrayPool = Preconditions.checkNotNull(byteArrayPool);
            this.parsers = Preconditions.checkNotNull(parsers);

            dataRewinder = new InputStreamRewinder(is, byteArrayPool);
        }


        @Override
        public PixelMap decodeBitmap(ImageSource.DecodingOptions options, String formatHint, boolean getimginfo) throws IOException {
            if (true == getimginfo) {
                Util.getImageInfo(dataRewinder.rewindAndGet(), options);
                return null;
            } else {
                return Util.decodePixelMap(dataRewinder.rewindAndGet(), options, formatHint);
            }
        }

        @Override
        public ImageHeaderParser.ImageType getImageType() throws IOException {
            return ImageHeaderParserUtils.getType(parsers, dataRewinder.rewindAndGet(), byteArrayPool);
        }

        @Override
        public int getImageOrientation() throws IOException {
            return ImageHeaderParserUtils.getOrientation(
                    parsers, dataRewinder.rewindAndGet(), byteArrayPool);
        }

        @Override
        public void stopGrowingBuffers() {
            dataRewinder.fixMarkLimits();
        }
    }


    final class ParcelFileDescriptorImageReader implements ImageReader {
        private final ArrayPool byteArrayPool;
        private final List<ImageHeaderParser> parsers;

        ParcelFileDescriptorImageReader(
                List<ImageHeaderParser> parsers,
                ArrayPool byteArrayPool) {
            this.byteArrayPool = Preconditions.checkNotNull(byteArrayPool);
            this.parsers = Preconditions.checkNotNull(parsers);

        }

    @Nullable
        @Override
        public PixelMap decodeBitmap(ImageSource.DecodingOptions options, String formatHint, boolean getimginfo) throws IOException {
           return null; //TODO: , check this
        }

        @Override
        public ImageHeaderParser.ImageType getImageType() throws IOException {
            return ImageHeaderParserUtils.getType(parsers, byteArrayPool);
        }

        @Override
        public int getImageOrientation() throws IOException {
            return ImageHeaderParserUtils.getOrientation(parsers, byteArrayPool);
        }

        @Override
        public void stopGrowingBuffers() {
            // Nothing to do here.
        }
    }
}
