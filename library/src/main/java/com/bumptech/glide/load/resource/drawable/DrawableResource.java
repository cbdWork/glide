package com.bumptech.glide.load.resource.drawable;

import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.util.LogUtil;
import com.bumptech.glide.util.Preconditions;
import ohos.agp.components.element.Element;
import org.jetbrains.annotations.NotNull;

/**
 * Simple wrapper for an openharmony {@link Element} which returns a
 * new drawable based on it's {@link Element.ConstantState state}.
 *
 * <p><b>Suggested usages only include {@code T}s where the new drawable is of the same or
 * descendant class.</b>
 *
 * @param <T> type of the wrapped {@link Element}
 */
public abstract class DrawableResource<T extends Element> implements Resource<T>, Initializable {
  protected final T drawable;

  public DrawableResource(T drawable) {
    this.drawable = Preconditions.checkNotNull(drawable);
  }

  @NotNull
  @SuppressWarnings("unchecked")
  @Override
  public final T get() {
    //TODO: getConstantState() not available
    return drawable;   
  }

  @Override
  public void initialize() {
    //TODO: preparetoDraw() not available
  }
}
