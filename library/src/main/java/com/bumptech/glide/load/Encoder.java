package com.bumptech.glide.load;

import org.jetbrains.annotations.NotNull;

import java.io.File;

/**
 * An interface for writing data to some persistent data store (i.e. a local File cache).
 *
 * @param <T> The type of the data that will be written.
 */
public interface Encoder<T> {
  /**
   * Writes the given data to the given output stream and returns True if the write completed
   * successfully and should be committed.
   *
   * @param data The data to write.
   * @param file The file to write the data to.
   * @param options The set of options to apply when encoding.
   * @return boolean
   */
  boolean encode(@NotNull T data, @NotNull File file, @NotNull Options options);
}
