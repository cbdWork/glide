package com.bumptech.glide.load.data;

import com.bumptech.glide.util.LogUtil;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.global.resource.NotExistException;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;
import org.jetbrains.annotations.NotNull;
import ohos.app.Context;
import java.io.*;

/**
 * Fetches an {@link InputStream} for a local {@link ohos.utils.net.Uri}.
 */
public class StreamLocalUriFetcher extends LocalUriFetcher<InputStream> {
    /**
     * A lookup uri
     */
    private static final int ID_CONTACTS_LOOKUP = 1;
    /**
     * A contact thumbnail uri
     */
    private static final int ID_CONTACTS_THUMBNAIL = 2;
    /**
     * A contact uri
     */
    private static final int ID_CONTACTS_CONTACT = 3;
    /**
     * A contact display photo (high resolution) uri
     */
    private static final int ID_CONTACTS_PHOTO = 4;
    /**
     * Uri for optimized search of phones by number
     */
    private static final int ID_LOOKUP_BY_PHONE = 5;

    private static final String DATAABILITY = "dataability";
    private static final String DATAABILITY_MEDIAIMAGES = "/media/external/images/media/";

    public StreamLocalUriFetcher(Context context, Uri uri) {
        super(context, uri);
    }


    @Override
    protected InputStream loadResource(Uri uri, Context context)
            throws FileNotFoundException {
        InputStream inputStream = loadResourceFromUri(uri, context);
        if (inputStream == null) {
            throw new FileNotFoundException("InputStream is null for " + uri);
        }
        return inputStream;
    }

    private InputStream loadResourceFromUri(Uri uri, Context context)
            throws FileNotFoundException {
        // TODO: consider contacts case also later
        FileInputStream fileInputStream =
                null; // TODO: need to handle based on URI.. need to parse URI
        DataAbilityHelper helper = null;
        ohos.global.resource.ResourceManager rm = context.getResourceManager();

        if ((null != uri.getScheme()) && uri.getScheme().equals(DATAABILITY)) {
            //It is in format of "dataability:///media/external/images/media/121"
            if (uri.getDecodedPath().startsWith(DATAABILITY_MEDIAIMAGES)) {

                try {
                    helper = DataAbilityHelper.creator(context);
                    FileDescriptor fd = helper.openFile(uri, "r");
                    FileInputStream fileInputStream1 = new FileInputStream(fd);
                    if (fileInputStream1 != null) {
                    }

                    return fileInputStream1;
                } catch (IOException | DataAbilityRemoteException exception) {
                    return null;
                }
            } else {
                //This is to load road resource with uri
                String path = uri.getDecodedPath();
                String resourcestr = path.substring(path.lastIndexOf('/') + 1);
                try {
                    return (InputStream) rm.getResource(Integer.parseInt(resourcestr)); //Resource is also a InputStream
                } catch (NotExistException | IOException exception) {
                    return null;
                }
            }
        } else {
            try {
                //Checking valid integer using parseInt() method
                String resourcestr = uri.getDecodedPath();

                try {
                    return (InputStream) rm.getResource(Integer.parseInt(resourcestr)); //Resource is also a InputStream
                } catch (NotExistException | IOException exception) {
                    return null;
                }

            } catch (NumberFormatException e) {
                //THen its not resource file
                try {
                    fileInputStream = new FileInputStream(uri.getDecodedPath());

                    return fileInputStream;
                } catch (IOException except) {
                }
            }
        }

        return null;
    }


    @Override
    protected void close(InputStream data) throws IOException {
        data.close();
    }

    @NotNull
    @Override
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }
}
