package com.bumptech.glide.load.resource.drawable;

import ohos.utils.net.Uri;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;

import java.io.InputStream;
import java.util.List;


public class ResourceDrawableDecoder implements ResourceDecoder<Uri, Element> {

  private final Context context;

  public ResourceDrawableDecoder(Context context) {
    this.context = context.getApplicationContext();
  }

  @Override
  public boolean handles( Uri source, Options options) {
    return true; //TODO
  }


  @Override
  public Resource<Element> decode(
      Uri source, int width, int height, Options options) {

    String path = source.getDecodedPath();
    String resourcestr = path.substring(path.lastIndexOf('/') + 1);
    int resId = Integer.parseInt(resourcestr);
    // We can't get a theme from another application.
    Element drawable = DrawableDecoderCompat.getDrawable(context, resId);
    return NonOwnedDrawableResource.newInstance(drawable);
  }

}
