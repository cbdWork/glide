package com.bumptech.glide.load.engine.prefill;

import com.bumptech.glide.util.LogUtil;

/**
 * A container for a put of options used to pre-fill a {@link
 * com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool} with {@link Bitmap Bitmaps} of a single
 * size and configuration.
 */
public final class PreFillType {
  private final int width;
  private final int height;
  private final int weight;

  PreFillType() {
    width =height =weight =0;
  }

  /** Returns the width in pixels of the Bitmaps.
   * @return int
   */
  int getWidth() {
    return width;
  }

  /** Returns the height in pixels of the Bitmaps.
   * @return int
   */
  int getHeight() {
    return height;
  }


  /** Returns the weight of the Bitmaps of this type.
   * @return int
   */
  int getWeight() {
    return weight;
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof PreFillType) {
      PreFillType other = (PreFillType) o;
      return height == other.height
          && width == other.width
          && weight == other.weight;
    }
    return false;
  }

  @Override
  public int hashCode() {
    int result = width;
    result = 31 * result + height;
    result = 31 * result + weight;
    return result;
  }

  @Override
  public String toString() {
    return "PreFillSize{"
        + "width="
        + width
        + "height="
        + height
        + ", weight="
        + weight
        + '}';
  }

  /** Builder for {@link PreFillType}. */
  public static class Builder {
    private final int width;
    private final int height;

    private int weight = 1;

    /**
     * Constructor for a builder that uses the given size as the width and height of the Bitmaps to
     * prefill.
     *
     * @param size The width and height in pixels of the Bitmaps to prefill.
     */
    public Builder(int size) {
      this(size, size);
    }

    /**
     * Constructor for a builder that uses the given dimensions as the dimensions of the Bitmaps to
     * prefill.
     *
     * @param width The width in pixels of the Bitmaps to prefill.
     * @param height The height in pixels of the Bitmaps to prefill.
     */
    public Builder(int width, int height) {
      if (width <= 0) {
        throw new IllegalArgumentException("Width must be > 0");
      }
      if (height <= 0) {
        throw new IllegalArgumentException("Height must be > 0");
      }
      this.width = width;
      this.height = height;
    }

    /**
     * Sets the weight to use to balance how many Bitmaps of this type are prefilled relative to the
     * other requested types.
     *
     * @param weight An integer indicating how to balance pre-filling this size and configuration of
     *     Bitmap against any other sizes/configurations that may be being
     *     pre-filled.
     * @return This builder.
     */
    public Builder setWeight(int weight) {
      if (weight <= 0) {
        throw new IllegalArgumentException("Weight must be > 0");
      }
      this.weight = weight;
      return this;
    }

    /** Returns a new {@link PreFillType}.
     * @return pre fill type */
    PreFillType build() {
      return new PreFillType();
    }
  }
}
