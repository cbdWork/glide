package com.bumptech.glide.load.data;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.util.LogUtil;
import org.jetbrains.annotations.NotNull;
import ohos.app.Context;
import java.io.IOException;

/**
 * An abstract class for obtaining data for an asset path using an {@link
 * context}.
 *
 * @param <T> The type of data obtained from the asset path (InputStream, FileDescriptor etc).
 */
public abstract class AssetPathFetcher<T> implements DataFetcher<T> {
  private static final String TAG = "AssetPathFetcher";
  private final String assetPath;
  private final Context context;
  private T data;

  // Public API.
  @SuppressWarnings("WeakerAccess")
  public AssetPathFetcher(Context context, String assetPath) {
    this.context = context; // TODO:
    this.assetPath = assetPath;
  }

  @Override
  public void loadData(@NotNull Priority priority, @NotNull DataCallback<? super T> callback) {
    try {
      data = loadResource(context, assetPath);
    } catch (IOException e) {
      if (LogUtil.isLoggable(TAG, LogUtil.DEBUG)) {
        LogUtil.info(TAG, "Failed to load data from asset manager");
      }
      callback.onLoadFailed(e);
      return;
    }
    callback.onDataReady(data);
  }

  @Override
  public void cleanup() {
    if (data == null) {
      return;
    }
    try {
      close(data);
    } catch (IOException e) {
      // Ignored.
    }
  }

  @Override
  public void cancel() {
    // Do nothing.
  }

  @NotNull
  @Override
  public DataSource getDataSource() {
    return DataSource.LOCAL;
  }

  /**
   * Opens the given asset path with the given context and returns
   * the concrete data type returned by the AssetManager.
   *
   * @param context An AssetManager to use to open the given path.
   * @param path A string path pointing to a resource in assets to open.
   * @return t
   * @throws IOException
   */
  protected abstract T loadResource(Context context, String path) throws IOException;

  /**
   * Closes the concrete data type if necessary.
   *
   * @param data The data to close.
   * @throws IOException
   */
  protected abstract void close(T data) throws IOException;
}
