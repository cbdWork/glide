package com.bumptech.glide.load.engine.bitmap_recycle;

import com.bumptech.glide.util.LogUtil;
import com.bumptech.glide.util.Synthetic;
import com.bumptech.glide.util.Util;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

/**
 * A strategy for reusing bitmaps that requires any returned bitmap's dimensions to exactly match
 * those request.
 */
class AttributeStrategy implements LruPoolStrategy {
  private final KeyPool keyPool = new KeyPool();
  private final GroupedLinkedMap<Key, PixelMap> groupedMap = new GroupedLinkedMap<>();
  private static final String TAG = "AttributeStrategy";

  @Override
  public void put(PixelMap pixelmap) {
    Size size =pixelmap.getImageInfo().size;
    final Key key = keyPool.get(size.width, size.height,pixelmap.getImageInfo().pixelFormat);

    groupedMap.put(key, pixelmap);
  }

  @Override
  public PixelMap get(int width, int height, PixelFormat config) {
    final Key key = keyPool.get(width, height, config);

    return groupedMap.get(key);
  }

  @Override
  public PixelMap removeLast() {
    return groupedMap.removeLast();
  }

  @Override
  public String logBitmap(PixelMap pixelmap) {
    return getPixelmapString(pixelmap);
  }

  @Override
  public String logBitmap(int width, int height, PixelFormat config) {
    return getPixelmapString(width, height, config);
  }

  @Override
  public int getSize(PixelMap pixelmap) {
    return Util.getBitmapByteSize(pixelmap);
  }

  @Override
  public String toString() {
    return "AttributeStrategy:\n  " + groupedMap;
  }

  private static String getPixelmapString(PixelMap pixelmap) {
    Size size =pixelmap.getImageInfo().size;

    return getPixelmapString(size.width, size.height, pixelmap.getImageInfo().pixelFormat);
  }

  @SuppressWarnings("WeakerAccess")
  @Synthetic
  static String getPixelmapString(int width, int height, PixelFormat config) {
    return "[" + width + "x" + height + "], " + config;
  }


  static class KeyPool extends BaseKeyPool<Key> {
    Key get(int width, int height, PixelFormat config) {
      Key result = get();
      result.init(width, height, config);
      return result;
    }

    @Override
    protected Key create() {
      return new Key(this);
    }
  }

  static class Key implements Poolable {
    private final KeyPool pool;
    private int width;
    private int height;
    // Config can be null :(
    private  PixelFormat config;

    public Key(KeyPool pool) {
      this.pool = pool;
    }

    public void init(int width, int height, PixelFormat config) {
      this.width = width;
      this.height = height;
      this.config = config;
    }

    @Override
    public boolean equals(Object o) {
      if (o instanceof Key) {
        Key other = (Key) o;
        return width == other.width && height == other.height && config == other.config;
      }
      return false;
    }

    @Override
    public int hashCode() {
      int result = width;
      result = 31 * result + height;
      result = 31 * result + (config != null ? config.hashCode() : 0);
      return result;
    }

    @Override
    public String toString() {
      return getPixelmapString(width, height, config);
    }

    @Override
    public void offer() {
      pool.offer(this);
    }
  }
}
