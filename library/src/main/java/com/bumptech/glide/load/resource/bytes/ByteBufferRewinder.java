package com.bumptech.glide.load.resource.bytes;

import com.bumptech.glide.load.data.DataRewinder;
import com.bumptech.glide.util.LogUtil;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;

/** Rewinds {@link ByteBuffer}s. */
public class ByteBufferRewinder implements DataRewinder<ByteBuffer> {
  private final ByteBuffer buffer;

  // Public API.
  @SuppressWarnings("WeakerAccess")
  public ByteBufferRewinder(ByteBuffer buffer) {
    this.buffer = buffer;
  }

  
  @NotNull
  @Override
  public ByteBuffer rewindAndGet() {
    buffer.position(0);
    return buffer;
  }

  @Override
  public void cleanup() {
    // Do nothing.
  }

  /** Factory for {@link com.bumptech.glide.load.resource.bytes.ByteBufferRewinder}. */
  public static class Factory implements DataRewinder.Factory<ByteBuffer> {

    @NotNull
    @Override
    public DataRewinder<ByteBuffer> build(ByteBuffer data) {
      return new ByteBufferRewinder(data);
    }

    @NotNull
    @Override
    public Class<ByteBuffer> getDataClass() {
      return ByteBuffer.class;
    }
  }
}
