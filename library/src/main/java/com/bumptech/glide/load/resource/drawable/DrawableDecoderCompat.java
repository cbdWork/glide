package com.bumptech.glide.load.resource.drawable;

import com.bumptech.glide.util.Util;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ColorSpace;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;
import ohos.global.resource.ResourceManager;

import java.io.IOException;
import java.io.InputStream;

/**
 * Handles decoding Drawables with the v7 support library if present and falling back to the v4
 * support library otherwise.
 */
public final class DrawableDecoderCompat {
  private static volatile boolean shouldCallAppCompatResources = true;

  private DrawableDecoderCompat() {
    // Utility class.
  }

  private static ImageSource.DecodingOptions getDefaultOptions() {
    ImageSource.DecodingOptions decodingOpts =new ImageSource.DecodingOptions();
    decodingOpts.desiredSize = new Size(0, 0);
    decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
    decodingOpts.desiredPixelFormat = PixelFormat.ARGB_8888;
    decodingOpts.allowPartialImage = false;
    decodingOpts.desiredColorSpace = ColorSpace.UNKNOWN;
    decodingOpts.sampleSize = 1;

    return decodingOpts;
  }


  public static Element getDrawable(
      Context ourContext, int id) {

    ResourceManager rm = ourContext.getResourceManager();
    try {
      InputStream is =rm.getResource(id);
      PixelMap pixelmap =Util.decodePixelMap(is, getDefaultOptions(), "image/png");
      return (Element)(new PixelMapElement( pixelmap));

    } catch (NotExistException | IOException exception) {
      return null;
    }
  }

}
