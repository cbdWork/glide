package com.bumptech.glide.load.engine.bitmap_recycle;

import com.bumptech.glide.util.LogUtil;
import com.bumptech.glide.util.Synthetic;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.VisibleForTesting;

/**
 * A strategy for reusing bitmaps that relies on {@link Bitmap#reconfigure(int, int,
 * Bitmap.Config)}.
 *
 * <p>Requires {@link Build.VERSION_CODES#KITKAT KitKat} or higher.
 */
//NOte: Glide does not use this file , it uses SizeConfigStrategy
final class SizeStrategy implements LruPoolStrategy {
  private static final int MAX_SIZE_MULTIPLE = 8;
  private final KeyPool keyPool = new KeyPool();
  private static final String TAG = "SizeStrategy";

  @Override
  public void put(PixelMap pixelMap) {
  }

  @Override
  @Nullable
  public PixelMap get(int width, int height, PixelFormat config) {
    return null;
  }

  @Override
  @Nullable
  public PixelMap removeLast() {
    return null;
  }


  private void decrementBitmapOfSize(Integer size) {
  }

  @Override
  public String logBitmap(PixelMap pixelMap) {
    return null;
  }

  @Override
  public String logBitmap(int width, int height, PixelFormat config) {
    return null;
  }

  @Override
  public int getSize(PixelMap pixelMap){
    return 0;
  }

  @Override
  public String toString() {
    return null;
  }

  private static String getBitmapString(PixelMap pixelMap) {
    return null;
  }

  @Synthetic
  static String getBitmapString(int size) {
    return "[" + size + "]";
  }

  // Non-final for mocking.
  @VisibleForTesting
  static class KeyPool extends BaseKeyPool<Key> {

    public Key get(int size) {
      return null;
    }

    @Override
    protected Key create() {
      return new Key(this);
    }
  }

 @VisibleForTesting
  static final class Key implements Poolable {
    private final KeyPool pool;
    @Synthetic int size;

    Key(KeyPool pool) {
      this.pool = pool;
    }

    public void init(int size) {
      this.size = size;
    }

    @Override
    public boolean equals(Object o) {
      if (o instanceof Key) {
        Key other = (Key) o;
        return size == other.size;
      }
      return false;
    }

    @Override
    public int hashCode() {
      return size;
    }

    // PMD.AccessorMethodGeneration: https://github.com/pmd/pmd/issues/807
    @SuppressWarnings("PMD.AccessorMethodGeneration")
    @Override
    public String toString() {
      return getBitmapString(size);
    }

    @Override
    public void offer() {
      pool.offer(this);
    }
  }
}
