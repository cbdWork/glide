package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.util.LogUtil;
import com.bumptech.glide.util.Preconditions;
import ohos.agp.components.element.PixelMapElement;
import ohos.media.image.PixelMap;
import org.jetbrains.annotations.NotNull;
import ohos.app.Context;

import java.io.IOException;

/**
 * Decodes an BitmapDrawable for a data type.
 *
 * @param <DataType> The type of data that will be decoded.
 */
public class BitmapDrawableDecoder<DataType>
        implements ResourceDecoder<DataType, PixelMapElement> {

    private final ResourceDecoder<DataType, PixelMap> decoder;
    private final Context context;

    @Deprecated
    public BitmapDrawableDecoder(
            Context context,
            @SuppressWarnings("unused") BitmapPool bitmapPool,
            ResourceDecoder<DataType, PixelMap> decoder) {
        this(context, decoder);
    }

    public BitmapDrawableDecoder(
            @NotNull Context context,
            @NotNull ResourceDecoder<DataType, PixelMap> decoder) {
        this.context = Preconditions.checkNotNull(context);
        this.decoder = Preconditions.checkNotNull(decoder);
    }

    @Override
  public boolean handles(@NotNull DataType source, @NotNull Options options) throws IOException {
        return decoder.handles(source, options);
    }

    @Override
    public Resource<PixelMapElement> decode(
      @NotNull DataType source, int width, int height, @NotNull Options options)
            throws IOException {
        Resource<PixelMap> bitmapResource = decoder.decode(source, width, height, options);
        return LazyBitmapDrawableResource.obtain(context, bitmapResource);
    }
}
