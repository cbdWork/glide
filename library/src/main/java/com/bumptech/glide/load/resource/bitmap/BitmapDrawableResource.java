package com.bumptech.glide.load.resource.bitmap;

import com.bumptech.glide.load.engine.Initializable;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.drawable.DrawableResource;
import com.bumptech.glide.util.LogUtil;
import com.bumptech.glide.util.Util;
import ohos.agp.components.element.PixelMapElement;
import org.jetbrains.annotations.NotNull;

/**
 * A {@link com.bumptech.glide.load.engine.Resource} that wraps an BitmapDrawable
 *
 * <p>This class ensures that every call to {@link #get()}} always returns a new BitmapDrawable
 * to avoid rendering issues if used in multiple views and
 * is also responsible for returning the underlying Bitmap to the given
 * {@link com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool} when the resource is recycled.
 */
public class BitmapDrawableResource extends DrawableResource<PixelMapElement>
    implements Initializable {
  private final BitmapPool bitmapPool;

  // Public API.
  @SuppressWarnings("WeakerAccess")
  public BitmapDrawableResource(PixelMapElement drawable, BitmapPool bitmapPool) {
    super(drawable);
    this.bitmapPool = bitmapPool;
  }

  
  @NotNull
  @Override
  public Class<PixelMapElement> getResourceClass() {
    return PixelMapElement.class;
  }

  @Override
  public int getSize() {
    return Util.getBitmapByteSize(drawable.getPixelMap());
  }

  @Override
  public void recycle() {
    bitmapPool.put(drawable.getPixelMap());
  }

  @Override
  public void initialize() {
    //drawable.getBitmap().prepareToDraw(); //TODO: openharmony has no support
  }
}
