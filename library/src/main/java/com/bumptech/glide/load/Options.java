package com.bumptech.glide.load;

import com.bumptech.glide.util.CachedHashCodeArrayMap;
import com.bumptech.glide.util.LogUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.MessageDigest;
import java.util.Map;

/** A set of {@link Option Options} to apply to in memory and disk cache keys. */
public final class Options implements Key {
  //Directly using CachedHashCodeArrayMap instead of LinkedHashMap since it doesnot provide getKeyAtIndex() and getValueAtIndex()
  private final CachedHashCodeArrayMap<Option<?>, Object> values = new CachedHashCodeArrayMap<>();

  public void putAll(@NotNull Options other) {
    values.putAll((Map<Option<?>, Object>) other.values);
  }

  
  @NotNull
  public <T> Options set(@NotNull Option<T> option, @NotNull T value) {
    values.put(option, value);
    return this;
  }

  @Nullable
  @SuppressWarnings("unchecked")
  public <T> T get(@NotNull Option<T> option) {
    return values.containsKey(option) ? (T) values.get(option) : option.getDefaultValue();
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Options) {
      Options other = (Options) o;
      return values.equals(other.values);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return values.hashCode();
  }

  @Override
  public void updateDiskCacheKey(@NotNull MessageDigest messageDigest) {
    for (int i = 0; i < values.size(); i++) {
      Option<?> key = values.keyAt(i);
      Object value = values.valueAt(i);
      updateDiskCacheKey(key, value, messageDigest);
    }
  }

  @Override
  public String toString() {
    return "Options{" + "values=" + values + '}';
  }

  @SuppressWarnings("unchecked")
  private static <T> void updateDiskCacheKey(
      @NotNull Option<T> option, @NotNull Object value, @NotNull MessageDigest md) {
    option.update((T) value, md);
  }
}
