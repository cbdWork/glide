package com.bumptech.glide;

import com.bumptech.glide.Glide.RequestOptionsFactory;
import com.bumptech.glide.load.engine.Engine;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.ImageViewTargetFactory;
import com.bumptech.glide.request.target.ViewTarget;
import com.bumptech.glide.util.LogUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.VisibleForTesting;
import ohos.app.Context;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Global context for all loads in Glide containing and exposing the various registries and classes
 * required to load resources.
 */
public class GlideContext{
  @VisibleForTesting
  static final TransitionOptions<?, ?> DEFAULT_TRANSITION_OPTIONS =
      new GenericTransitionOptions<>();
  private Context context;
  private final ArrayPool arrayPool;
  private final Registry registry;
  private final ImageViewTargetFactory harmonyImageViewTargetFactory;
  private final RequestOptionsFactory defaultRequestOptionsFactory;
  private final List<RequestListener<Object>> defaultRequestListeners;
  private final Map<Class<?>, TransitionOptions<?, ?>> defaultTransitionOptions;
  private final Engine engine;
  private final boolean isLoggingRequestOriginsEnabled;
  private final int logLevel;


  @Nullable
  private RequestOptions defaultRequestOptions;


  public GlideContext(
      @NotNull Context context,
      @NotNull ArrayPool arrayPool,
      @NotNull Registry registry,
      @NotNull ImageViewTargetFactory imageViewTargetFactory,
      @NotNull RequestOptionsFactory defaultRequestOptionsFactory,
      @NotNull Map<Class<?>, TransitionOptions<?, ?>> defaultTransitionOptions,
      @NotNull List<RequestListener<Object>> defaultRequestListeners,
      @NotNull Engine engine,
      boolean isLoggingRequestOriginsEnabled,
      int logLevel) {
    this.context = context;
    this.arrayPool = arrayPool;
    this.registry = registry;
    this.harmonyImageViewTargetFactory = imageViewTargetFactory;
    this.defaultRequestOptionsFactory = defaultRequestOptionsFactory;
    this.defaultRequestListeners = defaultRequestListeners;
    this.defaultTransitionOptions = defaultTransitionOptions;
    this.engine = engine;
    this.isLoggingRequestOriginsEnabled = isLoggingRequestOriginsEnabled;
    this.logLevel = logLevel;
  }

  public List<RequestListener<Object>> getDefaultRequestListeners() {
    return defaultRequestListeners;
  }

   public Context getContext() {
    return context;
  }

  public synchronized RequestOptions getDefaultRequestOptions() {
    if (defaultRequestOptions == null) {
      defaultRequestOptions = defaultRequestOptionsFactory.build().lock();
    }

    return defaultRequestOptions;
  }

  @SuppressWarnings("unchecked")
  @NotNull
  public <T> TransitionOptions<?, T> getDefaultTransitionOptions(@NotNull Class<T> transcodeClass) {
    TransitionOptions<?, ?> result = defaultTransitionOptions.get(transcodeClass);
    if (result == null) {
      for (Entry<Class<?>, TransitionOptions<?, ?>> value : defaultTransitionOptions.entrySet()) {
        if (value.getKey().isAssignableFrom(transcodeClass)) {
          result = value.getValue();
        }
      }
    }
    if (result == null) {
      result = DEFAULT_TRANSITION_OPTIONS;
    }
    return (TransitionOptions<?, T>) result;
  }


  @NotNull
  public <X> ViewTarget<ohos.agp.components.Image, X> buildHarmonyImageViewTarget(
      @NotNull ohos.agp.components.Image imageView, @NotNull Class<X> transcodeClass) {

    return harmonyImageViewTargetFactory.buildTarget(imageView, transcodeClass);
  }

  @NotNull
  public Engine getEngine() {
    return engine;
  }

  @NotNull
  public Registry getRegistry() {
    return registry;
  }

  public int getLogLevel() {
    return logLevel;
  }

  @NotNull
  public ArrayPool getArrayPool() {
    return arrayPool;
  }

  /**
   * Returns {@code true} if Glide should populate {@link
   * com.bumptech.glide.load.engine.GlideException#setOrigin(Exception)} for failed requests.
   *
   * <p>This is an experimental API that may be removed in the future.
   * @return boolean
   */
  public boolean isLoggingRequestOriginsEnabled() {
    return isLoggingRequestOriginsEnabled;
  }
}
