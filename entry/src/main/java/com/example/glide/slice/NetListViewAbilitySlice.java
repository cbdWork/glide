/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.glide.slice;

import com.example.glide.ResourceTable;
import com.example.glide.adapter.ModelBaseAdapter;
import com.example.glide.model.Model;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;


public class NetListViewAbilitySlice extends AbilitySlice {

    ListContainer list;
    Button button;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer)
                LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_list, null, false);
        setUIContent(rootLayout);
        list = (ListContainer) rootLayout.findComponentById(ResourceTable.Id_list);
        button = (Button) rootLayout.findComponentById(ResourceTable.Id_button);
        button.setText("Click Here");
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ModelBaseAdapter adapter = new ModelBaseAdapter(rootLayout.getContext(), inflateData());
                list.setItemProvider(adapter);
            }
        });

    }

    private List<Model> inflateData() {
        List<Model> mModelList = new ArrayList<Model>();

        mModelList.add(new Model("图片1", "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片2", "http://bookpic.lrts.me/4df8a19886644960a7299be21ac454ff.jpg"));
        mModelList.add(new Model("图片3", "http://bookpic.lrts.me/4df8a19886644960a7299be21ac454ff.jpg"));

        mModelList.add(new Model("百度新闻", "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片5",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));

        mModelList.add(new Model("图片6", "http://bookpic.lrts.me/4df8a19886644960a7299be21ac454ff.jpg"));
        mModelList.add(new Model("图片7", "http://bookpic.lrts.me/4df8a19886644960a7299be21ac454ff.jpg"));

        mModelList.add(new Model("图片8",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片9",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片10", "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片11", "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片12", "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片13", "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片14",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片15",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片16",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片17", "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片18",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片19",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片20",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("图片21",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));

        mModelList.add(new Model("图片6", "http://bookpic.lrts.me/4df8a19886644960a7299be21ac454ff.jpg"));
        mModelList.add(new Model("图片7", "http://bookpic.lrts.me/4df8a19886644960a7299be21ac454ff.jpg"));

        mModelList.add(new Model("图片22",	"https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));
        mModelList.add(new Model("百度新闻", "https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ericA1Mv66TwicuYOtbDMBcUhv1aa9RJBeAn9uURfcZD0AUGrJebAn1g2AjN0vb2E1XTET7fTuLBNmA/132"));

        return mModelList;
    }
}
