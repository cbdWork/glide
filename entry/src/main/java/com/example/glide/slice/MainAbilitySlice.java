package com.example.glide.slice;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GranularRoundedCorners;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.example.glide.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public class MainAbilitySlice extends AbilitySlice {
    private Button button, button2, button3, button4, button5, button6,button7;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);

        ComponentContainer rootLayout = (ComponentContainer) LayoutScatter.getInstance(this)
                .parse(ResourceTable.Layout_ability_main, null, false);

        button = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_1);
        button2 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_2);
        button3 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_3);
        button4 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_4);
        button5 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_5);
        button6 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_6);
        button7 = (Button) rootLayout.findComponentById(ResourceTable.Id_btn_7);

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                present(new GlideAbilitySlice(), new Intent());
            }
        });

        button2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                present(new GlideXmlAbilitySlice(), new Intent());
            }
        });

        button3.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                present(new TransformationsAbilitySlice(), new Intent());
            }
        });

        button4.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                present(new CustomTransformatinsAbilitySlice(), new Intent());
            }
        });

        button5.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                present(new ListViewTransAbilitySlice(), new Intent());
            }
        });

        button6.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

                present(new NetListViewAbilitySlice(), new Intent());
            }
        });

        button7.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                present(new GridAbilitySlice(),new Intent());
            }
        });

        super.setUIContent(rootLayout);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
