package com.example.glide.slice;

/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.example.glide.ResourceTable;
import com.example.glide.adapter.IntModelBaseAdapter;
import com.example.glide.model.IntModel;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import java.util.ArrayList;
import java.util.List;


public class ListViewTransAbilitySlice extends AbilitySlice {

    ListContainer list;
    Button button;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootLayout = (ComponentContainer)
                LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_view_list, null, false);
        setUIContent(rootLayout);
        list = (ListContainer) rootLayout.findComponentById(ResourceTable.Id_list);
        button = (Button) rootLayout.findComponentById(ResourceTable.Id_button);
        button.setText("Click Here");
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                IntModelBaseAdapter adapter = new IntModelBaseAdapter(rootLayout.getContext(), inflateData());
                list.setItemProvider(adapter);
            }
        });

    }

    private List<IntModel> inflateData() {
        List<IntModel> mModelList = new ArrayList<IntModel>();

        mModelList.add(new IntModel("图片1", ResourceTable.Media_flower));
        mModelList.add(new IntModel("图片2",	ResourceTable.Media_photo1));
        mModelList.add(new IntModel("图片3", ResourceTable.Media_photo2));
        mModelList.add(new IntModel("图片4", ResourceTable.Media_photo3));
        mModelList.add(new IntModel("图片5", ResourceTable.Media_photo4));
        mModelList.add(new IntModel("图片6", ResourceTable.Media_photo5));
        mModelList.add(new IntModel("图片7",	ResourceTable.Media_photo6));
        mModelList.add(new IntModel("图片8",	ResourceTable.Media_photo7));
        mModelList.add(new IntModel("图片9",	ResourceTable.Media_photo1));
        mModelList.add(new IntModel("图片10", ResourceTable.Media_photo2));
        mModelList.add(new IntModel("图片11", ResourceTable.Media_photo3));
        mModelList.add(new IntModel("图片12", ResourceTable.Media_photo4));
        mModelList.add(new IntModel("图片13", ResourceTable.Media_photo5));
        mModelList.add(new IntModel("图片14",	ResourceTable.Media_photo6));
        mModelList.add(new IntModel("图片15",	ResourceTable.Media_photo7));
        mModelList.add(new IntModel("图片16",	ResourceTable.Media_jellyfish));
        mModelList.add(new IntModel("图片17", ResourceTable.Media_tiger));
        mModelList.add(new IntModel("图片18",	ResourceTable.Media_photo1));
        mModelList.add(new IntModel("图片19",	ResourceTable.Media_photo2));
        mModelList.add(new IntModel("图片20",	ResourceTable.Media_photo3));
        mModelList.add(new IntModel("图片21",	ResourceTable.Media_photo7));
        mModelList.add(new IntModel("图片22",	ResourceTable.Media_jellyfish));
        mModelList.add(new IntModel("图片23", ResourceTable.Media_tiger));
        mModelList.add(new IntModel("图片24",	ResourceTable.Media_photo1));
        mModelList.add(new IntModel("图片25",	ResourceTable.Media_photo2));
        mModelList.add(new IntModel("图片26",	ResourceTable.Media_photo3));

        return mModelList;
    }
}

